from django.conf.urls import patterns, include, url

from .views import *

urlpatterns = patterns('',
  url(r'^$',                        ProductList(),   name='home'),
  url(r'^c/(?P<category>[\w-]+)/$', ProductList(),   name='home'),
  url(r'^(?P<product>[\w-]*)/$',    ComponentList(), name="product"),
)


