#
# Copyright (C) 2014, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.contrib.syndication.views import Feed
from django.shortcuts import get_object_or_404
from django.views.generic.base import View
from django.http import HttpResponse

from motte.views import *

from .models import *

class ProductList(CategoryListView):
    model = Product
    cats = (('category', "Project Categories"),)

class ComponentList(CategoryListView):
    model = Component
    opts = (('product', 'extra__slug'),)

