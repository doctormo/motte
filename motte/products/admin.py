#
# Copyright (C) 2014, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from django.contrib.admin import ModelAdmin, StackedInline, site
from .models import *
from .models import ProductExtra, ComponentExtra

site.register(Category)

class ProductExtraInline(StackedInline):
    model = ProductExtra
class ProductAdmin(ModelAdmin):
    inlines = (ProductExtraInline,)
site.register(Product, ProductAdmin)

class ComponentExtraInline(StackedInline):
    model = ComponentExtra
class ComponentAdmin(ModelAdmin):
    inlines = (ComponentExtraInline,)
site.register(Component, ComponentAdmin)

site.register(Version)
site.register(Milestone)

site.register(Status)
site.register(Severity)
site.register(Resolution)
site.register(Priority)
site.register(Platform)
site.register(Hardware)
site.register(Tag)

