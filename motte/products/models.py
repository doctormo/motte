#
# Copyright (C) 2014, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Original code Copyright 2014 by Xavier Ordoquy (Modified BSD License)
#
"""
Models to interact with bugzilla database
"""
# Notes:
#
# table - bugs_fulltext - Looks like a fulltext index, we'll use whoosh
# 

from __future__ import print_function, division
from __future__ import absolute_import, unicode_literals

from django.core.urlresolvers import reverse
from django.utils.encoding import python_2_unicode_compatible
from django.utils.text import slugify
from django.db.models import *

from auth.models import Profile
from settings import STATIC_URL
from fields import *
from queryset import *
 
import os

__all__ = ('Product', 'Component', 'Category', 'Version', 'Milestone',
           'Status', 'Resolution', 'Platform', 'Priority', 'Severity',
           'Hardware', 'Tag')

class Category(Model):
    name        = CharField(max_length=64, db_index=True, unique=True)
    description = TextField(blank=True, null=True)
    sortkey     = IntegerField(default=0)

    class Meta:
        db_table = 'classifications'

    def __str__(self):
        return self.name

    @property
    def value(self):
        return slugify(self.name)


class ProductManager(Manager):
    def get_query_set(self):
        return Manager.get_query_set(self).exclude(active=False)


class Product(Model):
    name      = CharField(max_length=64, db_index=True, unique=True)
    active    = BooleanIntField(default=True, db_column='isactive')
    milestone = ForeignKey('Milestone', blank=True, related_name='+', db_column='defaultmilestone')
    category  = ForeignKey(Category, db_column='classification_id', related_name='products')

    description  = TextField(blank=True, null=True)
    unconfirmed  = BooleanIntField(default=1, db_column='allows_unconfirmed')

    objects = ProductManager()

    class Meta:
        db_table = 'products'
        ordering = ['name']

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        if self.extra.pub_comp and self.components.count() >= 2:
            return reverse("product", kwargs={'product':self.extra.slug})
        return reverse("bugs", kwargs={'product':self.extra.slug})


class Component(Model):
    name    = CharField(max_length=64, db_index=True)
    product = ForeignKey(Product, related_name='components')
    owner   = ForeignKey(Profile, db_index=False,
                 db_column='initialowner', related_name='owned_components')
    assurer = ForeignKey(Profile, null=True, blank=True,
                 db_column='initialqacontact', related_name='watched_compononets')
    active  = BooleanIntField(db_column='isactive', default=True)

    description = TextField(default='', blank=True)
    subscribers = ManyToManyField(Profile, related_name='components',
                    through='ComponentCC', null=True, blank=True)

    class Meta:
        db_table = 'components'
        unique_together = ('name', 'product')

    def __str__(self):
        return self.name

    @property
    def value(self):
        return self.name

    def get_absolute_url(self):
        return reverse("bugs", kwargs={
            'product':self.product.extra.slug,
            'component':self.extra.slug})


class ComponentCC(Model):
    user      = ForeignKey(Profile, db_column='user_id', related_name='+')
    component = ForeignKey(Component, db_column='component_id', related_name='+')

    class Meta:
        db_table = 'component_cc'
        auto_created = Component


class Version(Model):
    value   = CharField(max_length=64, unique=True)
    product = ForeignKey(Product, related_name='versions', unique=True)
    active  = BooleanIntField(default=1, db_column='isactive')

    class Meta:
        db_table = 'versions'
        unique_together = ("value", "product")

    def __str__(self):
        return self.value


class Milestone(Model):
    product = ForeignKey(Product, related_name="milestones")
    value   = CharField(max_length=64, unique=True)
    sortkey = IntegerField(default=0)
    active  = BooleanIntField(default=1, db_column='isactive')

    class Meta:
        db_table = 'milestones'
        unique_together = ("value", "product")

    def __str__(self):
        return self.value

class BaseBugField(Model):
    value      = CharField(max_length=64, unique=True)
    sortkey    = IntegerField(default=0)
    active     = BooleanIntField(default=0, db_column='isactive')
    # This visibility field is clearly either a fixed choice or a ForeignKey
    #visibility = IntegerField(null=True, blank=True, db_column='visibility_value_id')

    class Meta:
        abstract = True

    def __str__(self):
        return self.value

    @property
    def field_id(self):
        return type(self).__name__.lower()


class Status(BaseBugField):
    is_open = BooleanIntField(default=True)

    class Meta:
        db_table = 'bug_status'
        ordering = ['sortkey', 'value']

    def __str__(self):
        return self.value.replace('_',' ').title()


class Severity(BaseBugField):
    class Meta:
        db_table = 'bug_severity'
        ordering = ['sortkey', 'value']

    def __str__(self):
        return self.value.title()


class Resolution(BaseBugField):
    class Meta:
        db_table = 'resolution'
        ordering = ['sortkey', 'value']

    def __str__(self):
        return self.value.replace('_',' ').title()


class Priority(BaseBugField):
    class Meta:
        db_table = 'priority'
        ordering = ['sortkey', 'value']

    def __str__(self):
        return "---" in self.value and "None" or self.value


class Platform(BaseBugField):
    class Meta:
        db_table = 'op_sys'
        ordering = ['sortkey', 'value']

    def __str__(self):
        return self.value


class Hardware(BaseBugField):
    class Meta:
        db_table = 'rep_platform'
        ordering = ['sortkey', 'value']

    def __str__(self):
        return self.value


class Tag(Model):
    name = CharField(max_length=64, db_index=True)
    user = ForeignKey(Profile, db_index=True)

    class Meta:
        db_table = 'tag'

    def __str__(self):
        return self.name


class BrandBase(Model):
    """Extend any model with slug and branding data"""
    slug    = CharField(max_length=64, db_index=True, unique=True)
    order   = IntegerField(default=0)

    large   = ResizedImageField("Large", null=True, blank=True,
                upload_to='brand/large', max_width=190, max_height=190)
    icon    = ResizedImageField("Icon", null=True, blank=True,
                upload_to='brand/icon', max_width=48, max_height=48)
    target_str = 'name'

    class Meta:
        abstract = True

    def __str__(self):
        return getattr(self.target, self.target_str, str(self.target))

    def save(self, *args, **kwargs):
        if self.target and not self.slug:
            self.slug = slugify(unicode(self))
        return Model.save(self, *args, **kwargs)

    @property
    def url(self):
        if self.large:
            return self.large.url
        return os.path.join(STATIC_URL, "images", "brand", "large.svg")

    @property
    def icon_url(self):
        if self.icon:
            return self.icon.url
        return os.path.join(STATIC_URL, "images", "brand", "icon.svg")


class ProductExtra(BrandBase):
    target   = AutoOneToOneField(Product, related_name='extra')
    pub_comp = BooleanField("Public Components", default=False)
    

class ComponentExtra(BrandBase):
    target = AutoOneToOneField(Component, related_name='extra')

