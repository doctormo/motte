
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from optparse import make_option

from products.models import *
from bugs.models import *

import logging
import sys
import os

from datetime import datetime
from bzlib.bugzilla import Bugzilla
from bzlib.config import Config

class MorphData(object):
    model  = None
    ignore = ()
    match  = ( ('id',), )
    same   = ()
    tr     = {}
    str_id = 'value'
    br     = {}

    def __init__(self):
        self._tr = dict( (value, key) for (key, value) in self.tr.items() )

    def get_field(self, name):
        ret = self.model._meta.get_field_by_name(name)
        return type(ret[0]).__name__, ret[0]

    def sub_morph(self, model, data):
        global MORPH
        morph = MORPH[model.__name__]
        if isinstance(data, basestring):
            data = {morph.str_id: data}
        if isinstance(data, dict):
            return morph(**data)
        else:
            raise TypeError("Dictionary expected for item: %s, got %s" % \
                (key, type(data).__name__))

    def sub_morphs(self, model, datas):
        if not isinstance(datas, (list, tuple)):
            raise TypeError("Expected a list for many-to-many %s got: %s" % \
                (key, type(datas).__name__))
        for data in datas:
            yield self.sub_morph(model, data)

    def filter(self, key, datum):
        try:
            return self._filter(key, datum)
        except Exception as error:
            #raise type(error)("%s=%s: %s" %(key, datum, str(error)))
            raise

    def _filter(self, key, datum):
        (ftype, field) = self.get_field(key)
        if ftype == 'ForeignKey':
            datum = self.sub_morph(field.rel.to, datum)
        elif ftype == 'RelatedObject':
            #datum = list(self.sub_morphs(field.model, datum))
            # We need to do more here to make this work!
            return None, None
        #elif ftype == 'ManyToManyField':
        #elif ftype == 'OneToOneField':
        elif ftype in ('AutoField','IntegerField'):
            datum = int(datum)
        elif 'Bool' in ftype:
            datum = bool(datum)
        elif ftype in ('CharField', 'TextField'):
            pass
        else:
            raise KeyError("Unkown Filter: %s: %s" % (ftype, str(field)))
            pass # Regular filter here?

        return key, datum

    def remove(self, data, keys, strict=False):
        for key in keys:
            n_key = self._tr.get(key, key)
            if n_key in data:
                yield self.filter(key, data.pop(n_key))
            elif strict:
                raise KeyError("Key '%s' (was: %s) not found in data %s" % (n_key, key, str(data)))

    def _remove(self, data, keys):
        return dict( (name, data.pop(name)) for name in keys if name in data )

    def get_matcher(self, data):
        for matcher in self.match:
            if not [ name for name in matcher if name not in data ]:
                return matcher
        return False

    def does_not_exist(self, data, error):
        raise error

    def __call__(self, **data):
        self._remove(data, self.ignore)
        self.data = data.copy()

        matcher = self.get_matcher(data)
        if not matcher:
            raise KeyError("There's nothing to match: %s" % str(data))
        match = dict( self.remove(data, matcher, True) )
        if len(match) != len(matcher):
            raise KeyError("Column removal returned nothing: %s from %s (got %s)" % (str(matcher), str(data.keys()), str(match)))

        try:
            return self.model.objects.get(**match)
        except self.model.DoesNotExist as error:
            if not data:
                self.does_not_exist(match, error)
        fields  = self.model._meta.get_all_field_names()
        lates   = self._remove(data, self.br.keys())
        columns = match
        columns.update( dict( self.remove(data, fields) ) )
        columns.pop(None, None)
        print columns
        obj = self.model(**columns)
        if data:
            raise KeyError("Values not been dealt with: %s" % ', '.join(data.keys()))
        print "Saving object: %s" % type(obj).__name__
        self.save(obj, **lates)
        return obj

    def save(self, obj, **kw):
        obj.save()

class ProductMorph(MorphData):
    model = Product
    tr    = {
      'classification':    'category',
      'default_milestone': 'milestone',
      'is_active':         'active',
      'has_unconfirmed':   'unconfirmed',
    }
    br = { 'default_milestone': 'product' }

    def save(self, obj, default_milestone, **kw):
        obj.milestone = Milestone.objects.get()
        obj.save()
        # XXX Save a list of items from the related filter...
        #if default_milestone:
        # Save single items for milestone here, once it exists
        return obj

class CategoryMorph(MorphData):
    model = Category
    str_id = 'name'
    match = ( ('name',), )


class VersionMorph(MorphData):
    model  = Version
    ignore = ('sort_key',)
    match = ( ('name', 'product') )
    tr     = {
      'is_active': 'active',
      'name'     : 'value',
    }

class MilestoneMorph(MorphData):
    model  = Milestone
    match = ( ('id'), ('value', 'product') )
    tr    = {
      'is_active': 'active',
      'name': 'value',
      'sort_key': 'sortkey',
    }

class ComponentMorph(MorphData):
    model = Component
    ignore = ('sort_key',)
    match = ( ('name',), )
    tr    = {
      'is_active': 'active',
      'default_qa_contact': 'assurer',
      'default_assigned_to': 'owner',
    }
    
class BugMorph(MorphData):
    model = Bug
    ignore = ('is_open','classification',)
    tr = {
      'summary':          'title',
      'creation_time':    'created',
      'last_change_time': 'modified',
      'op_sys':           'platform',
      'creator':          'reporter',
      'is_confirmed':     'confirmed',
      'is_creator_accessible': 'can_report',
      'target_milestone': 'milestone',
    } 

class CommentMorph(MorphData):
    model = Comment

class FieldMorph(MorphData):
    model = FieldDef
    tr = {}

#  {
#  'is_custom': False,
#  'is_mandatory': False,
#  'name': 'priority',
#  'id': 13,
#  'values': [
#    {'sort_key': 100, 'visibility_values': [], 'name': 'highest', 'sortkey': 100},
#    {'sort_key': 200, 'visibility_values': [], 'name': 'high', 'sortkey': 200},
#    {'sort_key': 300, 'visibility_values': [], 'name': 'medium', 'sortkey': 300},
#    {'sort_key': 400, 'visibility_values': [], 'name': 'low', 'sortkey': 400},
#    {'sort_key': 500, 'visibility_values': [], 'name': 'lowest', 'sortkey': 500}
#  ],
#  'visibility_values': [],
#  'type': 2,
#  'is_on_bug_entry': False,
#  'display_name': 'Priority'
#  }

#class FieldValueMorph(MorphData):
#    model = FieldValueDef
#    tr = {}

class ProfileMorph(MorphData):
    model = Profile
    ignore = 'email',
    tr = {
      'can_login': 'is_enabled',
      'name':      'username',
      'real_name': 'realname',
    }

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
#        make_option('-e', '--example', dest='foo', help='Bar'),
      )

    def __init__(self, *args, **kwargs):
        BaseCommand.__init__(self, *args, **kwargs)
        conf = Config.get_config('~/.bugzillarc')
        self.bz = Bugzilla.from_config(conf, server=None, url=None, user=None, password=None)

    def populate_projects(self):
        for p in self.bz.get_products():
            MORPH['Product'](**p)

    def populate_fields(self):
        for f in self.bz.get_fields():
            sys.stderr.write("Field: %s\n" % str(f))
            break

    def get_user(self, user='doctormo'):
        u = self.bz.match_users(user)
        sys.stderr.write("User: %s\n" % str(u))

    def populate_bug(self, bug_id):
        b = self.bz.match_bug(bug_id)
        sys.stderr.write("Bug: %s\n" % str(b))

    def handle(self, *args, **op):
#        self.get_user()
#        self.populate_fields()
        self.populate_projects()
        return
        _bug = 1 # start_bug
        while _bug:
            bug = self.populate_bug(_bug)
            if d is None:
                if str(_bug).strip('0') == '1':
                    _bug *= 10
                else:
                    _bug = None
                continue


MORPH = {}
for (name, attr) in locals().items():
    if hasattr(attr, 'model') and attr.model:
        MORPH[attr.model.__name__] = attr()
