#
# Copyright (C) 2014, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Index our products, components and bugs.
"""

from haystack.indexes import *

from motte.search_base import add_fields

from .models import Product, Component
from .views import ProductList, ComponentList

class ProductIndex(SearchIndex, Indexable):
    text = CharField(document=True, use_template=True)

    def get_model(self):
        return Product

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(active=True)


class ComponentIndex(SearchIndex, Indexable):
    text = CharField(document=True, use_template=True)

    def get_model(self):
        return Component

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(active=True)


# Our bug should index fields also used in the categorisation
add_fields(ProductIndex, ProductList)
add_fields(ComponentIndex, ComponentList)

