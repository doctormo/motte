#
# Copyright (C) 2014, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Provides as very good filtering system with automatic category generation
for ListView. This allows multiple categories to be specified and for them
to automatically filter the list.

Supports both url based key words e.g. /(?<category>\w+)/ and
get kwargs on the request. Define the urls with the right category id
to get automatic url based atrtirubution.

Example in views.py:

class MyView(CategoryListView):
    model = my_model
    cats = ('status', _("Status")),\
           ('type', _("Type")),\
           ('foo', _("Bar"), Baz),
    opts = ('user', 'username'),

in urls.py:

url('^$', MyView.as_view(), name='myview')
url('^(?P<type>[\w-]+)/$', MyView.as_view(), name='myview')

In this example our ListView is created as normal with a model.
It also specifies a cats class attribute which defines each category
and it's title that will appear in the template.

Status is a ForeignKey on my_model and this will generate a list of selectable
status items which when selected will filter my_model via it's status=item link
The value of status is passed in via the request.GET dictionary. Generated urls
for each selectable item will include the attribute after the quest mark

e.g. /url/?status=value

Type does the same as status, but instead of the attribute existing in the GET
dictionary, it will be passed in via the url kwargs. These are automatically
detected so there isn't any further boilerplait code.

e.g. /type1/

foo acts differently. Where status and type are ForeignKey fields and the linked
model is detected automatically. foo isn't a foreignkey field. We pass in the
categories model which will be listed and the lookup is done on it's direct value
instead of it's link id.

The code depends on a 'value' field or object property existing on each of the
category objects in order to get it's 'slug' name rather than it's display name.

opts contains similar filtering as cats, but are not displayed. They are either
selected by previous/other pages or are selected via simple links which reset
all other category selections.

This allows filtering of fields that don't appear in the category list.

The CategoryFeed allows the same categories to be turned into an rss feed.

All View classes don't need as_view() and can be created directly from urls.
"""

__all__ = ('get_url', 'CategoryListView', 'CategoryFeed')

from django.contrib.syndication.views import Feed
from django.views.generic import CreateView, DetailView, FormView
from django.views.generic.base import TemplateView as View, View as BaseView
from django.core.urlresolvers import reverse, get_resolver
from django.shortcuts import get_object_or_404, redirect
from django.utils.http import urlquote

import types
import sys

#
# Note: We understand we are playing with fire when we change the way the django view
# interface works, but these modifications make the API much easier to use and it is
# my fondest hope that such improvements go into future django versions.
#

def _as_view_call(self, request, *args, **kwargs):
    """Remove the need for as_view() in urls"""
    if hasattr(self, 'get') and not hasattr(self, 'head'):
        self.head = self.get
    self.request = request
    self.args = args
    self.kwargs = kwargs
    return self.dispatch(request, *args, **kwargs)

def get_url(name, *args, **kwargs):
    return reverse(name, kwargs=kwargs, args=args)

for (name, arg) in locals().items():
    # MOKEY-PATCHING: Insert the caller into any view without a caller
    if isinstance(arg, (type, types.ClassType)) and issubclass(arg, BaseView):
        arg.__call__ = _as_view_call
        if 'Base' not in name:
            __all__ += name,

class AllCategory(object):
    """A simple object for 'All' Menu Item"""
    value = None

    def __str__(self):
        return "All" # XXX i18n

def _delay(method, *args, **kwargs):
    def internal():
        return method(*args, **kwargs)
    return internal

class Category(list):
    """A menu of items in this category, created by ListView"""
    def __init__(self, view, queryset, cid, name=None):
        self.value = view.get_value(cid)

        self.name = name or cid.replace('_',' ').title()
        self.cid  = cid
        self.item = AllCategory()
        # Populate items, mostly these models don't have slug columns.
        self.append( self.item )
        for item in queryset:
            if self.value is not None and item.value == self.value:
                self.item = item
            self.append(item)
        for item in self:
            item.url = _delay(view.get_url, cid, item.value)
            item.count = _delay(view.get_count, cid, item)

    def count(self):
        """Returns the number of active items"""
        return len(list(item for item in self if item.count())) - 1

    def __str__(self):
        return self.name

    def __eq__(self, value):
        return self.item == value

    def __nonzero__(self):
        return self.item != self[0]


def clean_dict(a):
    """Removes any keys where the values is None"""
    for key in a.keys():
        if a[key] is None:
            a.pop(key)
    return a


def cached(f):
    """We save the details per request"""
    key = '_' + f.__name__
    def _call(self, *args, **kwargs):
        target = self.request
        field = key
        if args:
            field += '_' + ('_'.join(a for a in args if isinstance(a, basestring)))
        if not hasattr(target, field):
            ret = f(self, *args, **kwargs)
            if isinstance(ret, types.GeneratorType):
                ret = list(ret)
            setattr(target, field, ret)
        return getattr(target, field)
    return _call

# This is all required because whoost/haystack is as dumb as a post
# about logical OR between tokens in the query string.
import re
exact_match_re = re.compile(r'(?P<phrase>[-+]?".*?")')
def get_query(model, query):
    from haystack.query import SearchQuerySet, AutoQuery
    queryset = SearchQuerySet().models(model)
    exacts = exact_match_re.findall(query)
    tokens = []
    for t in exact_match_re.split(query):
        if t in exacts:
            tokens.append(t) # add quotes back
        else:
            tokens += t.split()
    for token in tokens:
        if token[0] in '+-':
            queryset = queryset.filter_and(content=AutoQuery(token.lstrip('+')))
        else:
            queryset = queryset.filter_or(content=AutoQuery(token))
    return queryset


class CategoryListView(View):
    """ListView with categorisation functionality, provides a simple way to
    Define a set of categories and have them availab ein the template with urls"""
    cats = ()
    opts = ()
    rss_view = None

    def get_queryset(self, **kwargs):
        self.query = self.request.GET.get('q', None)
        def _get(cat):
            return hasattr(self.model, cat.cid) and cat.item or cat.item.value

        if self.query:
            queryset = get_query(self.model, self.query)
        else:
            queryset = self.model._default_manager.all()
        filters = dict((cat.cid, _get(cat)) for cat in self.get_cats() if cat)

        filters.update(clean_dict(dict(self.get_opts())))
        filters.update(kwargs)
        sys.stderr.write("Filters: %s\n" % str(filters))
        return queryset.filter(**clean_dict(filters))

    def get_url(self, cid=None, value=None, view=None, exclude=None):
        kwargs = self.kwargs.copy()
        gets = self.request.GET.copy()
        view = view or self.request.resolver_match.url_name
        if cid is not None:
            args = self.get_possible_args(view)
            target = cid in args and kwargs or gets
            if value is None:
                target.pop(cid, None)
            else:
                target[cid] = value
        url = get_url(view, **kwargs)
        if gets:
            # Always remove page, start from begining
            gets.pop('page', None)
            get = ('&'.join('%s=%s' % (a,urlquote(b)) for (a,b) in gets.items() if a != exclude))
            if get:
                url += '?' + get
        return url

    @cached
    def get_possible_args(self, view_name):
        """Returns a generator with all possible kwargs for this view name"""
        resolver = get_resolver(None)
        possibilities = resolver.reverse_dict.getlist(view_name)
        for possibility, pattern, defaults in possibilities:
            for r, params in possibility:
                for p in params:
                    # Remove non-keyword arguments
                    if p[0] != '_':
                        yield p

    def get_count(self, cid, item=None, value=None):
        if not hasattr(self.model, cid):
            item = item.value
        if item is None or getattr(item, 'value', item) is None:
            item = None
        return self.get_queryset(**{cid: item}).count()

    def get_value(self, cid):
        return self.kwargs.get(cid, self.request.GET.get(cid, None))

    @cached
    def get_opts(self):
        return [self.get_opt(*opt) for opt in self._opts()]

    @classmethod
    def _opts(cls):
        """Returns a set of filtered links for manually defined options"""
        for (cid, link) in cls.opts:
            field = cid
            if '__' in link:
                (nfield, rest) = link.split('__', 1)
                if not hasattr(cls.model, field)\
                   and hasattr(cls.model, nfield):
                    (field, link) = (nfield, rest)
            yield (cid, link, field)
        # Yield extra options here via automatic association?

    def get_opt(self, cid, link, field, context=False):
        """Returns a value suitable for filtering"""
        value = self.get_value(cid)
        if self.query and not context:
            # No object lookup for haystack search
            return (cid, value)

        if value is not None:
            if hasattr(self.model, field):
                mfield = getattr(self.model, field)
                try:
                    values = mfield.get_query_set().filter(**{link: value})
                    if values.count() == 1:
                        value = values[0]
                    else:
                        value = [ v for v in values ]
                        field = field + '__in'
                except mfield.field.rel.to.DoesNotExist:
                    value = None
            elif link and not context:
                raise ValueError("Can't find the model for field: %s.%s" % (self.model.__name__, link))
        return (context and cid or field, value)

    @cached
    def get_value_opts(self):
        """Similar to get_opt, but returns value useful for templates"""
        return [self.get_opt(*opt, context=True) for opt in self._opts() ]

    @cached
    def get_cats(self):
        return [ self.get_cat(*cat) for cat in self.cats ]

    def get_cat(self, cid, name, model=None):
        # We could move this into Category class XXX
        if hasattr(self.model, cid):
            qs = getattr(self.model, cid).get_query_set()
        elif model:
            qs = model.objects.all()
        else:
            raise KeyError(("The field '%s' isn't a ForeignKey, please add"
                           "the linked model for this category.") % cid)
        return Category(self, qs, cid, name)

    def get(self, *args, **kwargs):
        # XXX There is going to be subtle problems here between
        # When we want items to redirect and when we don't.
        qs = self.get_queryset()
        if not self.query and qs.count() == 1:
            item = qs.get()
            if hasattr(item, 'get_absolute_url'):
                return redirect( item.get_absolute_url() )
        context = self.get_context_data(object_list=qs)
        return self.render_to_response(context)

    def get_template_names(self):
        opts = self.model._meta
        return ["%s/%s_list.html" % (opts.app_label, opts.object_name.lower())]
 
    def get_context_data(self, **data):
        # this allows search results and object queries to work the same way.
        if not hasattr(self.model, 'object'):
            self.model.object = lambda self: self

        data['query'] = self.query
        data['categories'] = self.get_cats()
        data.update(((cat.cid, cat.item) for cat in self.get_cats() if cat))
        data.update(self.get_value_opts())
        
        if self.rss_view:
            data['rss_url'] = self.get_url(view=self.rss_view)
        data['clear_url'] = self.get_url(exclude='q')
        return data


class CategoryFeed(Feed):
    """So these CategoryListViews can become Feeds we have to do a few bits for django"""
    def __call__(self, request, *args, **kwargs):
        self.request = request
        self.kwargs = kwargs
        return Feed.__call__(self, request, *args, **kwargs)

    def items(self):
        ret = self.get_queryset()
        if self.order_by:
            ret = ret.order_by(self.order_by)
        return ret

    def link(self):
        return self.get_url()

    # XXX it might be possible to generate a title and description here.

