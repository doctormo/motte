#
# Copyright (C) 2014, Martin Owens
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from urlparse import urlparse

from django.shortcuts import get_object_or_404, render_to_response, redirect
from django.contrib.auth import login, logout, forms
from django.template import RequestContext
from django.conf import settings


from django.http.response import HttpResponseRedirectBase
from django.core.exceptions import SuspiciousOperation
class HttpResponseRedirectStrict(HttpResponseRedirectBase):
    status_code = 302
    def __init__(self, redirect_to, *args, **kwargs):
        parsed = urlparse(redirect_to)
        if not settings.DEBUG \
           and parsed.netloc and parsed.netloc not in settings.ALLOWED_HOSTS:
            raise SuspiciousOperation("Unsafe redirect to URL with host '%s'" % parsed.netloc)
        super(HttpResponseRedirectStrict, self).__init__(redirect_to, *args, **kwargs)


# Note: It was a lot of trouble to put these two functions into View classes
# And it ended up being way too much trouble. We're waiting for contrib.auth
# to be upgraded with easy to use view classes of it's own
def do_login(request, *args, **kwargs):
    if request.method == 'POST':
        form = forms.AuthenticationForm(data=request.POST)
        if form.is_valid():
            login(request, form.get_user())
            # We could redirect to a failure page here, but meh
            dest = request.REQUEST.get('next', settings.LOGIN_REDIRECT_URL)
            return HttpResponseRedirectStrict(
                request.REQUEST.get('next', settings.LOGIN_REDIRECT_URL)
            )
    else:
        form = forms.AuthenticationForm()

    return render_to_response('auth/login.html', {
        'form': form,
        'next': request.REQUEST.get('next', None),
     }, context_instance=RequestContext(request))


def do_logout(request, *args, **kwargs):
    if request.user.is_authenticated():
        logout(request)
    dest = request.META.get('HTTP_REFERER') or '/'
    return HttpResponseRedirectStrict(dest)

