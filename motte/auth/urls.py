from django.conf.urls import patterns, include, url

from .views import *

urlpatterns = patterns('',
  url(r'^login/$',   do_login,  name='login'),
  url(r'^logout/$',  do_logout, name='logout'),
#  url(r'^$',         Profile(), name='profile'),
)


