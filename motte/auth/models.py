#
# copyright: (c) 2013-2014 by Xavier Ordoquy
# license: Modified BSD
#

from __future__ import print_function, division
from __future__ import absolute_import, unicode_literals

from django.utils.encoding import python_2_unicode_compatible
from django.contrib.auth.models import BaseUserManager
from django.contrib.auth import hashers
from django.db.models import *

from fields import BooleanIntField

# Make sure to import the .hashers
from . import hashers as local_hashers


@python_2_unicode_compatible
class MyGroup(Model):
    name         = CharField(max_length=255, db_index=True)
    description  = TextField()

    is_active    = BooleanIntField(db_column='isactive', default=1)
    is_bug_group = BooleanIntField(db_column='isbuggroup')
    icon_url     = CharField(max_length=255, blank=True, null=True)
    userregexp   = CharField(max_length=255, blank=True)

    class Meta:
        db_table = 'groups'

    def __str__(self):
        return self.name


class ProfileManager(BaseUserManager):
    pass


@python_2_unicode_compatible
class Profile(Model):
    id       = AutoField(primary_key=True, db_column='userid')
    username = CharField(max_length=255, db_index=True, unique=True, db_column='login_name')
    password = CharField(max_length=128, null=True, db_column='cryptpassword')
    realname = CharField(max_length=255, default='')

    disabledtext   = TextField(default='', blank=True)
    disable_mail   = BooleanIntField(default=0, blank=True)
    mybugslink     = BooleanIntField(default=1, blank=True)
    extern_id      = CharField(max_length=64, null=True, blank=True,
                                            db_index=True)
    is_enabled     = BooleanIntField(default=1, blank=True)
    last_seen_date = DateTimeField(null=True, blank=True) # timestamp without timezone

    groups = ManyToManyField(MyGroup, related_name='users', through="GroupMap", blank=True, null=True)

    REQUIRED_FIELDS = []
    USERNAME_FIELD = 'username'

    objects = ProfileManager()

    def __str__(self):
        if self.realname:
            return self.realname
        return self.get_username()

    class Meta:
        db_table = 'profiles'

    def get_absolute_url(self):
        return '' # XXX

    def is_active(self):
        return bool(self.disabledtext)

    def get_username(self):
        return self.username

    def natural_key(self):
        return (self.get_username(),)

    def is_anonymous(self):
        """
        Always returns False. This is a way of comparing User objects to
        anonymous users.
        """
        return False

    def is_authenticated(self):
        """
        Always return True. This is a way to tell if the user has been
        authenticated in templates.
        """
        return True

    def set_password(self, raw_password):
        self.password = hashers.make_password(raw_password)

    def check_password(self, raw_password):
        """
        Returns a boolean of whether the raw_password was correct. Handles
        hashing formats behind the scenes.
        """
        def setter(raw_password):
            self.set_password(raw_password)
            self.save(update_fields=["password"])
        return hashers.check_password(raw_password, self.password, setter)

    def set_unusable_password(self):
        # Sets a value that will never be a valid hash
        self.password = '*'

    def has_usable_password(self):
        return hashers.is_password_usable(self.password)

    def get_full_name(self):
        return self.realname

    def get_short_name(self):
        return self.extern_id

    #
    # HACKS
    # TODO: implement those 3 functions.
    # For now this is enough to test the legacy authentication
    #
    def is_staff(self):
        return True

    def is_superuser(self):
        return True

    def has_module_perms(self, app_label):
        return True

    def has_perm(self, perm):
        return True


class GroupMap(Model):
    user       = ForeignKey(Profile)
    group      = ForeignKey(MyGroup)
    isbless    = BooleanIntField(default=False)
    grant_type = IntegerField(default=0)

    class Meta:
        db_table = 'user_group_map'
        auto_created = Profile

class Watch(Model):
    watcher = ForeignKey(Profile, db_column='watcher', related_name='watches')
    watched = ForeignKey(Profile, db_column='watched', related_name='watchers')

    class Meta:
        db_table = 'watch'

# not used yet:
# setting
# setting_value
# profile_setting
# profile_search
# profile_activity
# group_group_map
# group_control_map

from django.contrib.auth.signals import user_logged_in
from django.contrib.auth.models import update_last_login

user_logged_in.disconnect(update_last_login)
