#
# Copyright (C) 2014, Martin Owens
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

SECRET_KEY = 'z07po59sz(60s&i04ry^(74)u*#$+x7y(0f6*)@35%7kpexax9'
TEMPLATE_DEBUG = True
DEBUG = True

ALLOWED_HOSTS = ['localhost']

PASSWORD_HASHERS = (
    'auth.hashers.BugzillaHasher',
)

# Application definition
INSTALLED_APPS = (
    'django_reset',
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'haystack',
    'pagination',
    'auth',
    'products',
    'bugs',
    'tagcss',
)

AUTH_USER_MODEL = 'auth.Profile'

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'pagination.middleware.PaginationMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.i18n',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
)

ROOT_URLCONF = 'urls'

WSGI_APPLICATION = 'wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'data', 'bugzilla.db'),
#        'ENGINE': 'django.db.backends.postgresql_psycopg2',
#        'NAME': 'bugs',
#        'USER': 'bugs',
#        'PASSWORD': '123456',
#        'HOST': 'localhost',
    }
}
HAYSTACK_CONNECTIONS = {
    # haystack >= 2.0
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(BASE_DIR, 'data', 'search'),
    },
}

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True
USE_L10N = True
USE_TZ = True

SITE_ID = 1

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'data', 'media')

STATIC_URL = '/static/'
STATICFILES_DIRS = os.path.join(BASE_DIR, 'static'),

TEMPLATE_DIRS = os.path.join(BASE_DIR, 'templates'),

LOGIN_URL = '/profile/login/'

# ==== SETUP DB STEP 2 ==== #

from django.db.backends.signals import connection_created

def collate(string1, string2):
    return cmp(string1, string2)

def add_collation(sender, connection, **kwargs):
    """The database needs a bugzilla collections signal"""
    connection.connection.create_collation(str("bugzilla"), collate)

# This extra step is required for sqlite3 database backends
# because bugzilla added a collate method which we need to patch in.
if 'sqlite' in DATABASES['default']['ENGINE']:
    connection_created.connect(add_collation)

