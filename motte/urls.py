
from django.conf.urls import patterns, include, url
from django.conf import settings

from django.views.generic import TemplateView
from django.views import static

from django.contrib import admin, staticfiles

from tagcss.views import CssTagList

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^tag.css$',CssTagList.as_view(), name='tag_css'),

    url(r'^auth/',   include('auth.urls')),
    url(r'^admin/',  include(admin.site.urls)),
    url(r'^about/',  TemplateView.as_view(template_name="about.html"), name='about'),
    url(r'',         include('products.urls')),
    url(r'',         include('bugs.urls')),
)

if settings.DEBUG:
    urlpatterns += [
        url(r'^static/(?P<path>.*)$', staticfiles.views.serve),
        url(r'^media/(?P<path>.*)$', static.serve,
             {'document_root': settings.MEDIA_ROOT}),
    ]

from django.contrib.sites.models import Site

if 'example' in str(Site.objects.get()) and not settings.DEBUG:
    raise ValueError("You need to set the site object before turrning off debug.")
