#
# Copyright (C) 2014, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Returns the right style name for any kind of tag (status, priority, etc)
"""

from django.conf import settings
from django.template.base import Library
from django.utils.safestring import mark_safe

from motte.tagcss.models import TagCssEntry

register = Library()

@register.filter("tagstyle", is_safe=False)
def style_filter(value):
    ret = TagCssEntry.objects.filter(value__iexact=value)
    return ret and ret[0].name or ''

