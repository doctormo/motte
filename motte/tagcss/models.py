#
# Copyright (C) 2014, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Original code Copyright 2014 by Xavier Ordoquy (Modified BSD License)
#
"""
Provides a way to colour-code the various labels and tags.
"""

from django.utils.text import slugify
from django.db.models import *

import os

TEXT_STYLES = (
  ('font-weight:bold', 'Bold'),
  ('text-decoration:underline', 'Underlined'),
  ('text-decoration:line-through', 'Striked'),
  ('font-style:italic', 'Italic'),
)

CSS_TAG_PREFIX = 'tag-style'

class TagCssEntry(Model):
    value        = CharField(max_length=64)
    slug         = CharField(max_length=64)

    colour       = CharField(max_length=6, blank=True, null=True)
    background   = CharField(max_length=6, blank=True, null=True)
    display      = BooleanField(default=True)
    text_style   = CharField(max_length=64, blank=True, null=True, choices=TEXT_STYLES)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.value)
        return Model.save(self, *args, **kwargs)

    @property
    def name(self):
        return "%s-%s" % (CSS_TAG_PREFIX, self.slug)

    @property
    def style(self):
        ret = ''
        if self.colour:
            ret += '  color: #%s;\n' % self.colour
        if self.background:
            ret += '  background-color: #%s;\n' % self.background
        if self.display == False:
            ret += '  display: none;\n'
        if self.text_style:
            ret += '  %s;\n' % self.text_style
        if ret:
            return '.%s {\n%s}\n' % (self.name, ret)
        return ''

    def __str__(self):
        return self.slug

