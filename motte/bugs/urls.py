from django.conf.urls import patterns, include, url

from .views import *

def url_tree(regex, *urls):
    return url(regex, include(patterns('', *urls)))

urlpatterns = patterns('',
  # Download from the daft attachment blob in the database
  url(r'^d/(?P<pk>\d+)/(?P<filename>.*)$', download, name='download'),

  url_tree(r'(?P<pk>\d+)',
     url(r'^$',                     BugItem(), name='bug'),
     # url(r'^/edit/$',              EditBug(), name='bug.edit'),
     # url(r'^/delete/$',            DeleteBug(), name='bug.delete'),
     # url(r'^/comment/$',           AddToBug(), name='bug.comment'),
  ),
  url(r'(?P<bug>\d+)/(?P<pk>\d+)/', Comment(), name='comment'),

  url_tree(r'^(?P<product>[\w-]+)/',
     url(r'^all/$',                       BugList(),       name="bugs"),
     url(r'^all/rss/$',                   BugFeed(),       name="bugs_rss"),
     url(r'^add/$',                       ReportBug(),     name="report_bug"),
     url(r'^(?P<component>[\w-]+)/$',     BugList(),       name="bugs"),
     url(r'^(?P<component>[\w-]+)/rss/$', BugFeed(),       name="bugs_rss"),
     url(r'^(?P<component>[\w-]+)/add/$', ReportBug(),     name="report_bug"),
  ),
)


