#
# Copyright (C) 2014, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Original code Copyright 2014 by Xavier Ordoquy (Modified BSD License)
#
"""
Models to interact with bugzilla database
"""
# Notes:
#
# table - bugs_fulltext - Looks like a fulltext index, we'll use whoosh
# 

from __future__ import print_function, division
from __future__ import absolute_import, unicode_literals

from django.core.urlresolvers import reverse
from django.utils.encoding import python_2_unicode_compatible
from django.db.models import *

from compositekey.db import MultiFieldPK 

from auth.models import Profile
from settings import STATIC_URL, STATICFILES_DIRS
from fields import *
from queryset import *
 
from products.models import *

import os

__all__ = ('Bug', 'BugDependencies', 'BugActivity', 'FieldDef', 'Comment',
           'Attachment')

# TODO:
# Manage the field type #7
# Manage the field type #3

CF_TYPE_CHOICES = (
    (0, 'Unknown'),
    (1, 'Free text'),
    (2, 'Single select'),
    (3, 'Multi select'),
    (4, 'Text area'),
    (5, 'Datetime'),
    (6, 'Bug id'),
    (7, 'Bug urls'),
)

CF_TYPE_MAPPING = [
    None,
    (CharField, {'max_length': 255, 'null': True, 'blank': True}),
    (CharField, {'max_length': 255, 'default': '---'}),
    None,
    (TextField, {'null': True, 'blank': True}),
    (DateTimeField, {'null': True, 'blank': True}),
    (IntegerField, {}),
    None,
]


class FieldDef(Model):
    name         = CharField(max_length=64)
    type         = IntegerField(default=0, choices=CF_TYPE_CHOICES)
    description  = CharField(max_length=255)
    long_desc    = CharField(max_length=255, default='')
    reverse_desc = CharField(max_length=255, blank=True, null=True)
    sortkey      = IntegerField()

    custom       = BooleanIntField(default=0)
    mailhead     = BooleanIntField(default=0)
    obsolete     = BooleanIntField(default=0)
    enter_bug    = BooleanIntField(default=0)
    buglist      = BooleanIntField(default=0)
    mandatory    = BooleanIntField(default=0, db_column='is_mandatory')
    numeric      = BooleanIntField(default=0, db_column='is_numeric')

    visibility   = ForeignKey('self', blank=True, null=True, related_name='+', db_column='visibility_field_id')
    value        = ForeignKey('self', blank=True, null=True, related_name='+', db_column='value_field_id')

    class Meta:
        db_table = 'fielddefs'

    def __str__(self):
        return self.name


class BugQuerySet(ManagerQuerySet):
    def opened(self):
        opened_status = Status.objects.filter(is_open=1)
        return self.filter(status__in=[s.value for s in opened_status])

    def closed(self):
        closed_status = Status.objects.filter(is_open=0)
        return self.filter(status__in=[s.value for s in closed_status])


class BaseBug(Model):
    """Bugzilla Bug"""
    id         = AutoField(primary_key=True, db_column='bug_id')
    alias      = CharField(max_length=20, db_index=True, unique=True, null=True, blank=True)
    title      = CharField(max_length=255, db_column='short_desc')
    assignee   = ForeignKey(Profile, db_column='assigned_to')
    product    = ForeignKey(Product, db_index=True)
    component  = ForeignKey(Component)

    # XXX These values require an id which is paired with the product id, this makes them broken
    # at the moment as it means Version and Milstone models won't accept the same value for two different products.
    version    = ForeignKey(Version,   'value', db_index=True, blank=True, db_column='version')
    milestone  = ForeignKey(Milestone, 'value', db_index=True, blank=True, db_column='target_milestone')

    # XXX The null contraint is weird here, db columns say blank is ok but null is not. But django converts blank
    # strings to null and thus values can't be unset (even though they ort to be)
    severity   = ForeignKey(Severity,   'value', db_index=True, blank=True, db_column='bug_severity')
    status     = ForeignKey(Status,     'value', db_index=True, blank=True, db_column='bug_status')
    resolution = ForeignKey(Resolution, 'value', db_index=True, blank=True, db_column='resolution')
    priority   = ForeignKey(Priority,   'value', db_index=True, blank=True, db_column='priority')
    platform   = ForeignKey(Platform,   'value', db_index=True, blank=True, db_column='op_sys')
    hardware   = ForeignKey(Hardware,   'value', db_index=True, blank=True, db_column='rep_platform')

    location   = TextField(default='', blank=True, db_column='bug_file_loc')
    cc_list    = BooleanIntField(default=True, db_column='cclist_accessible')

    created    = DateTimeField(db_index=True, null=True, db_column='creation_ts')
    modified   = DateTimeField(db_index=True, db_column='delta_ts')
    lastdiffed = DateTimeField(null=True, blank=True, default=None)
    deadline   = DateTimeField(null=True, blank=True)

    estimated  = DecimalField(max_digits=5, decimal_places=2, default='0', db_column='estimated_time')
    remaining  = DecimalField(max_digits=5, decimal_places=2, default='0', db_column='remaining_time')
    confirmed  = BooleanIntField(default=False, db_column='everconfirmed')
    reporter   = ForeignKey(Profile, db_column='reporter', related_name='created_bugs')

    whiteboard = TextField(default='', db_column='status_whiteboard', blank=True)
    can_report = BooleanIntField(default=True, db_column='reporter_accessible')

    depends_on  = ManyToManyField('self', related_name='blocks', through='BugDependencies',
                    null=True, blank=True, symmetrical=False)
    subscribers = ManyToManyField(Profile, related_name='subscribed_bugs', through='BugCC',
                    null=True, blank=True)
    tags        = ManyToManyField(Tag, related_name="bugs", db_table='bug_tag',
                    null=True, blank=True)
    duplicate   = ManyToManyField('self', related_name='duplicates', through='BugDuplicates',
                    null=True, blank=True, symmetrical=False)

    objects    = BugQuerySet().as_manager()

    class Meta:
        abstract = True

    def __str__(self):
        return 'Bug #%s - %s' % (self.id, self.title)

    def get_absolute_url(self):
        return reverse("bug", kwargs={'pk':self.id})

    def is_open(self):
        try:
            return bool(Status.objects.get(value=self.status).is_open)
        except Status.DoesNotExist:
            return True


def create_bug_model(class_name='Bug', options=None):
    """Enrich the BaseBug to use the custom bugzilla fields"""
    # Inspiration from https://code.djangoproject.com/wiki/DynamicModels
    class Meta:
        db_table = 'bugs'
        app_label = 'bugs'

    # Set up a dictionary to simulate declarations within a class
    attrs = {'__module__': 'bugs', 'Meta': Meta}

    # Add in any fields that were provided
    try:
        for field_def in FieldDef.objects.filter(custom=True, obsolete=False):
            try:
                FieldClass, options = CF_TYPE_MAPPING[field_def.type]
            except TypeError:
                print('Skipping field %s' % field_def)
                continue
            attrs[field_def.name] = FieldClass(**options)
    except Exception as e:
        print('Exception caught: %s' % str(e))

    # Create the class, which automatically triggers ModelBase processing
    return type(str(class_name), (BaseBug,), attrs)

Bug = create_bug_model()

class DependManager(Manager):
    def open(self):
        """Return true if any bug is open"""
        return self.get_query_set().filter(blocked__is_open=True)

class BugDependencies(Model):
    id         = MultiFieldPK("blocked", "depends_on")
    blocked    = ForeignKey(Bug, db_column='blocked', related_name='+')
    depends_on = ForeignKey(Bug, db_column='dependson', related_name='+')

    objects    = DependManager()

    class Meta:
        db_table = 'dependencies'
        auto_created = Bug

    def __str__(self):
        return '%s -> %s' % (str(self.depends_on), str(self.blocked))

class BugDuplicates(Model):
    id      = MultiFieldPK("dupe", "dupe_of")
    dupe    = ForeignKey(Bug, db_column='dupe', related_name='+')
    dupe_of = ForeignKey(Bug, db_column='dupe_of', related_name='+')

    class Meta:
        db_table = "duplicates"
        auto_created = Bug

class BugCC(Model):
    id    = MultiFieldPK("bug", "who")
    bug   = ForeignKey(Bug, db_column='bug_id', related_name='+')
    who   = ForeignKey(Profile, db_column='who', related_name='+')

    class Meta:
        db_table = 'cc'
        auto_created = Bug

    def __str__(self):
        return '%i -> %i' % (self.who_id, self.bug_id)


class BugSeeAlso(Model):
    bug        = ForeignKey(Bug, related_name='urls')
    value      = CharField("URL", max_length=255)
    perl_class = CharField("URL", max_length=255, db_column='class')

    class Meta:
        db_table = 'bug_see_also'
        auto_created = Bug

    def type(self):
        return self.perl_class.split('::')[-1].lower()


class CommentManager(Manager):
    def get_query_set(self):
        return Manager.get_query_set(self).exclude(content='').order_by('when')

    def rest(self):
        return self.get_query_set()[1:]

    def first(self):
        lquery = self.get_query_set()
        if lquery:
            return lquery[0]
        return None


CMT_TYPES = (
  (0, 'Normal'),
  (1, 'Is a Duplicate of'),
  (2, 'Has a New Duplicate'),
  (5, 'Attachment Added'),
  (6, 'Attachment Updated'),
)

class Comment(Model):
    id         = AutoField(primary_key=True, db_column='comment_id')
    bug        = ForeignKey(Bug, related_name='comments', db_index=True)
    who        = ForeignKey(Profile, to_field='id', db_column='who', db_index=True)
    when       = DateTimeField(auto_now_add=True, db_column='bug_when', db_index=True)
    work_time  = DecimalField(default='0.00', max_digits=5, decimal_places=2)
    content    = TextField(db_column='thetext')
    private    = BooleanIntField(default=False, db_column='isprivate')
    wrapped    = BooleanIntField(default=False, db_column="already_wrapped")
    ctype      = IntegerField(default=0, db_column="type")
    extra_data = CharField(max_length=255, null=True, blank=True)

    # Extra data contains attachment id if ctype is 5 or 6, weird huh
    objects     = CommentManager()

    class Meta:
        db_table = 'longdescs'

    def __str__(self):
        return self.content


class BugActivity(Model):
#    bug     = ForeignKey(Bug, related_name='activity')
    attach  = ForeignKey("Attachment", related_name='activity', null=True, blank=True)
    who     = ForeignKey(Profile, db_column='who', related_name='activity')
    when    = DateTimeField(db_column='bug_when')
    fieldid = IntegerField()
    added   = CharField(max_length=255, blank=True, null=True)
    removed = CharField(max_length=255, blank=True, null=True)
    comment = ForeignKey(Comment, related_name='activity', blank=True, null=True)

    class Meta:
        db_table = "bugs_activity"

    def __str__(self):
        return self.fieldid


# We investigated using a storage mechanism here with the ability to use FileFields
# But because of it's structre, the OneToOne relationship fits and we will have to
# maually control how data gets inserted from the bug's attachment add forms.
class AttachmentData(Model):
    data = BinaryField(db_column='thedata')

    class Meta:
        db_table = "attach_data"

class Attachment(Model):
    file        = OneToOneField(AttachmentData, primary_key=True, db_column='attach_id')

#    bug         = ForeignKey(Bug, related_name='attachments')
    created     = DateTimeField(db_column='creation_ts')
    modified    = DateTimeField(db_column='modification_time', blank=True)

    description = TextField(blank=True)
    mimetype    = TextField(blank=True)
    filename    = CharField(max_length=255)
    submitter   = ForeignKey(Profile, related_name='attachments')
    ispatch     = BooleanIntField(default=False)
    isobsolete  = BooleanIntField(default=False)
    isprivate   = BooleanIntField(default=False)

    def size(self):
        return len(self.file.data)

    class Meta:
        db_table = "attachments"

    def get_absolute_url(self):
        return reverse('download', kwargs={
            'pk': self.pk,
            'filename': self.filename,
          })

    def icon_url(self):
        mime = self.mimetype.split('/')[0]
        for fn in ["%s.svg" % mime, 'unknown']:
            path = os.path.join(STATICFILES_DIRS[0], "images", "mime", fn)
            if os.path.exists(path):
                return os.path.join(STATIC_URL, "images", "mime", fn)

    def __str__(self):
        return self.description


