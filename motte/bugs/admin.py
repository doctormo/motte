#
# Copyright (C) 2014, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from django.contrib.admin import ModelAdmin, StackedInline, site
from .models import *
from .forms import *

site.register(FieldDef)
site.register(BugDependencies)
site.register(Comment)
site.register(BugActivity)
site.register(Attachment)

class BugAdmin(ModelAdmin):
    readonly_fields = ('subscribers', 'depends_on')
    list_display = ('title', 'pk', 'product', 'status', 'resolution')
    form = BugAdminForm

site.register(Bug, BugAdmin)
