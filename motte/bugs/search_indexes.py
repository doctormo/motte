#
# Copyright (C) 2014, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Index our products, components and bugs.
"""

from haystack.indexes import *

from motte.search_base import add_fields

from .models import Bug
from .views import BugList

class BugIndex(SearchIndex, Indexable):
    text     = CharField(document=True, use_template=True)
    modified = DateTimeField(model_attr='modified')

    def get_model(self):
        return Bug

    def get_updated_field(self):
        return 'modified'

    def index_queryset(self, using=None):
        return self.get_model().objects.all()

# Our bug should index fields also used in the categorisation
add_fields(BugIndex, BugList)

