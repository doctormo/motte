#
# Copyright (C) 2014, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.contrib.syndication.views import Feed

from motte.views import *

from .forms import *
from .models import *

def download(request, pk, filename):
    attachment = get_object_or_404(Attachment, pk=pk, filename=filename)
    return HttpResponse(
        attachment.file.data,
        mimetype=attachment.mimetype
    )

class BugList(CategoryListView):
    model = Bug
    cats = (('severity', "Severity"),
            ('status', "Status"),
           )
    opts = (('product', 'extra__slug'),
            ('component', "extra__slug"),
            ('reporter', 'username'),
            ('open', 'status__is_open'),
           )
    rss_view = 'bugs_rss'

    def get_context_data(self, **kwargs):
        data = CategoryListView.get_context_data(self, **kwargs)
        args = {'product': data['product'].extra.slug}
        if data.get('component', None):
            args['component'] = data['component'].extra.slug
        data['report_url'] = get_url('report_bug', **args)
        return data



class BugFeed(CategoryFeed, BugList):
    title = "Bug Feed"
    description = "Bugs"
    order_by = '-modified'

    # Title and description should be defined by the CategoryListView concatination.

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.comments.first().content

    def item_guid(self, item):
        return '#'+str(item.pk)

    def item_author_name(self, item):
        return str(item.reporter)

    def item_author_link(self, item):
        return item.reporter.get_absolute_url()

    def item_pubdate(self, item):
        return item.created

    def item_updateddate(self, item):
        return item.modified


class BugItem(DetailView):
    model = Bug

    def get_context_data(self, **kwargs):
        data = DetailView.get_context_data(self, **kwargs)
        data['product'] = data['object'].product
        data['component'] = data['object'].component
        return data

class Comment(DetailView):
    model = Comment

    def get_queryset(self):
        qs = DetailView.get_queryset(self)
        if 'bug' in self.kwargs:
            qs = qs.filter(bug_id=self.kwargs['bug'])
        return qs

from products.models import Product, Component

class ReportBug(FormView):
    form_class = BugForm

    def get_template_names(self):
        return 'bugs/bug_report.html',

    def get_context_data(self, *args, **kwargs):
        data = FormView.get_context_data(self, **kwargs)
        data['product'] = Product.objects.get(extra__slug=self.kwargs['product'])
        #data['component'] = Component.objects.get(extra__slug=data['component'])
        data['title'] = self.request.POST.get('title', None)
        if self.request.POST.get('search', None) == 'true':
            data['items'] = self.search_bugs(data['title'])
        return data

    def search_bugs(self, terms):
        # TODO: do a search here
        return [
{'title':'Some Bug Reported Ages ago', 'get_stored_fields':{'text': """This is a bug test, a test which includes a very long pieces of data on more than one line.

First we have the main body of the content which should include various bits and bobs. Then we continue with more bits of text. We need so much text because the test bug will pick out a few parts of the bug to show to the user with various highlights in place.

Thus it's very important that this bug report is as long as possible to show what should be possible in the report.
"""}},
{'title':'We were not aware of the problem', 'get_stored_fields':{'text': """Man does not understand well enough most of the test bug reports he is assigned.

So long as this is always the case, we will always have that to fear.
"""}},
{'title':'The Ice Ageis Coming', 'get_stored_fields':{'text': """I know they make great films and everything. But an Ice Age is no joking matter.

We should make sure to provision the right items and not make references to see what happens.
"""}},
        ]

